// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'default e2e tests': function(browser) {
    // automatically uses dev Server port from /config.index.js
    // default: http://localhost:8080
    // see nightwatch.conf.js
    const devServer = browser.globals.devServerURL

    browser
      .url(devServer)
      .waitForElementVisible('#app', 5000)
      .assert.elementPresent('input[type=text]')
      .assert.elementPresent('input[type=password]')
      .assert.elementPresent('button[type=submit]')
      .setValue('input[type=text]', 'cristobalramos')
      .setValue('input[type=password]', 'password')
      .click('button[type=submit]')
      .pause(5000)
      .assert.elementPresent('form.crear-asistencia')
      .assert.elementPresent('aside.navigation-drawer')
      .assert.elementPresent('nav.toolbar')

    browser
      .url(devServer + '/clientes/crear')
      .waitForElementVisible('#app', 5000)
      .assert.elementPresent('input[aria-label=RUT]')
      .assert.elementPresent('input[aria-label=Nombre legal]')
      .assert.elementPresent('input[aria-label=Nombre ficticio]')
      .assert.elementPresent('input[aria-label=Teléfono]')
      .assert.elementPresent('input[aria-label=Correo]')
      .assert.elementPresent('button[type=submit]')
      .setValue('input[aria-label=RUT]', '181898593')
      .setValue('input[aria-label=Nombre legal]', 'Cristóbal Ramos')
      .setValue('input[aria-label=Nombre ficticio]', 'Cristóbal')
      .setValue('input[aria-label=telefono]', '2894033')
      .setValue('input[aria-label=correo]', 'cristobal@ramos.cl')
      .click('button[type=submit]')
      .end()
  },
}
