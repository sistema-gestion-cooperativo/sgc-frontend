import Vue from 'vue'
import Vuetify from 'vuetify'
import ListaBalances from '@/components/Balances/ListaBalances'
import VerBalance from '@/components/Balances/VerBalance'

Vue.use(Vuetify)

describe('Balances', () => {
  it('debe listar los balances', () => {
    const Constructor = Vue.extend(ListaBalances)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('.datatable.table').textContent)
      .to.equal('Welcome to Your Vue.js App')
  })
  it('debe mostrar los balances individuales', () => {
    const Constructor = Vue.extend(VerBalance)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('.datatable.table').textContent)
      .to.equal('Welcome to Your Vue.js App')
  })
})