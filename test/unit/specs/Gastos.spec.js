import Vue from 'vue'
import Vuetify from 'vuetify'
import ListaGastos from '@/components/Cotizaciones/ListaGastos'
import CrearGasto from '@/components/Cotizaciones/CrearGasto'

Vue.use(Vuetify)

describe('listado de gastos', () => {
  it('debe mostrar el centro de costos del gasto', () => {
  })
  it('debe mostrar el monto total del gasto', () => {
  })
  it('debe mostrar la fecha del gasto', () => {
  })
  it('debe mostrar al responsable del gasto', () => {
  })
  it('debe mostrar al proveedor del gasto', () => {
  })
})

describe('creación de gastos', () => {
  it('el monto total debe ser igual a la suma de pagos', () => {

  })
})