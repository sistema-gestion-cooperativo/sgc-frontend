import axios from '@/utils/http.js'
import Cookies from 'js-cookie'
const env = require('@/.env.js')

const state = {
  expires_in: null,
  access_token: null,
  refresh_token: null,
  token_type: null,
  client_id: '2',
  client_secret: env.client_secret,
}

const mutations = {
  CAMBIAR_EXPIRES_IN(state, expires_in) {
    state.expires_in = expires_in
  },
  CAMBIAR_ACCESS_TOKEN(state, access_token) {
    state.access_token = state.token_type + ' ' + access_token
  },
  CAMBIAR_REFRESH_TOKEN(state, refresh_token) {
    state.refresh_token = state.token_type + ' ' + refresh_token
  },
  CAMBIAR_TOKEN_TYPE(state, token_type) {
    state.token_type = token_type
  },
}

const actions = {
  cambiarAutenticacion(context, data) {
    context.commit('CAMBIAR_TOKEN_TYPE', data.token_type)
    context.commit('CAMBIAR_EXPIRES_IN', data.expires_in)
    context.commit('CAMBIAR_ACCESS_TOKEN', data.access_token)
    context.commit('CAMBIAR_REFRESH_TOKEN', data.refresh_token)
  },
  autenticar(context, { usuario, contrasena }) {
    return new Promise((resolve, reject) => {
      axios
        .post('/oauth/token', {
          grant_type: 'password',
          client_id: context.getters.clientId,
          client_secret: context.getters.clientSecret,
          username: usuario,
          password: contrasena,
          scope: '',
        })
        .then(response => {
          context.dispatch('cambiarAutenticacion', response.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  cerrarSesion(context) {
    window.localStorage.removeItem('sgc') //TODO: Hay más llaves que se crean para guardar datos en local
    window.sessionStorage.removeItem('sgc') //TODO: Hay más llaves que se crean para guardar datos en local
    Cookies.remove('priv')
    window.location.reload()
  },
}

const getters = {
  estaAutenticado(state) {
    //TODO: Esto solo prueba que el token tenga letras, no verifica que coincida con el del servidor
    return !!state.access_token
  },
  refreshToken(state) {
    return state.refresh_token
  },
  accessToken(state) {
    return state.access_token
  },
  clientId(state) {
    return state.client_id
  },
  clientSecret(state) {
    return state.client_secret
  },
}

export default {
  state,
  actions,
  mutations,
  getters,
}
