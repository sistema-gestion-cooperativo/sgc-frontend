import axios from '@/utils/http.js'

const state = {
  unidadProductivaUsuario: null,
  personaUsuario: null,
  menuUsuario: null,
}

const mutations = {
  CAMBIAR_UNIDAD_PRODUCTIVA(state, unidad_productiva) {
    state.unidadProductivaUsuario = unidad_productiva
  },
  CAMBIAR_PERSONA(state, persona) {
    state.personaUsuario = persona
  },
  CAMBIAR_MENU(state, menu) {
    state.menuUsuario = menu
  },
}

const actions = {
  cambiarPerfil(context) {
    return new Promise((resolve, reject) => {
      axios
        .get('/api/perfil')
        .then(response => {
          context.commit(
            'CAMBIAR_UNIDAD_PRODUCTIVA',
            response.data.data.unidad_productiva
          ) //TODO: Comprobar si el campo attibutes es canonico con el resto de modelos (al paracer es JSON:API, pero no se usa en Laravel)
          context.commit('CAMBIAR_PERSONA', response.data.data.persona)
          context.commit('CAMBIAR_MENU', response.data.data.menu)

          //TODO: if response.status <> 200 reject()
          resolve(response.data.data)
        })
        .catch(error => {
          context.dispatch('agregarError', {
            titulo: 'perfil.js',
            error
          })
          reject(error)
        })
    })
  },
}

const getters = {
  nombreUnidadProductivaUsuario: state => {
    return state.unidadProductiva ?
      state.unidadProductiva.nombre_presentacion :
      'SGC'
  },
  aclPermission: state => state.personaUsuario ? state.personaUsuario.rol.codigo : 'publico'
}

export default {
  state,
  actions,
  mutations,
  getters,
}
