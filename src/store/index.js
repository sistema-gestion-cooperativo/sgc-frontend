import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import Cookies from 'js-cookie'
import perfil from './modules/perfil'
import interfaz from './modules/interfaz'
import autenticacion from './modules/autenticacion'
import asistencia from './modules/asistencia'

Vue.use(Vuex)
let vuexLocal = createPersistedState({
  key: 'sgc',
  paths: [
    'perfil',
    'interfaz',
    'asistencia',
  ],
}),
  vuexCookie = createPersistedState({
    key: 'priv',
    paths: ['autenticacion'],
    storage: {
      getItem: key => Cookies.get(key),
      setItem: (key, value) => Cookies.set(key, value, { expires: 3, secure: true }),
      removeItem: key => Cookies.remove(key)
    }
  })

const store = new Vuex.Store({
  modules: {
    perfil,
    interfaz,
    autenticacion,
    asistencia,
  },
  plugins: [vuexLocal, vuexCookie],
})

export default store
