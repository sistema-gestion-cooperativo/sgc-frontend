// const SolicitarVacaciones = () =>
//   import('@/components/Remuneraciones/SolicitarVacaciones')

// const CrearOportunidad = () => import('@/components/CrearOportunidad')
// const CrearOpinion = () => import('@/components/CrearOpinion')
/**
 * Administración
 */
// const ListaBalances = () => import('@/components/Balances/ListaBalances')

// const SolicitarFondoPorRendir = () =>
//   import('@/components/FondosPorRendir/SolicitarFondoPorRendir')

/*
Reglas:
- esAdministracion
- esComercial
- esRecursosHumanos
- esProduccion
- esJefeDeObra
- esSocio
- esTrabajador
- esPublico
*/

const routes = [{
    path: '/',
    redirect: {
      name: 'escritorio',
    },
    meta: {
      title: 'Cargando',
      rule: 'esPublico',
    },
  },
  {
    path: '/ingreso',
    name: 'ingreso',
    component: () => import('@/components/Ingreso'),
    meta: {
      title: 'Ingreso',
      rule: 'esPublico',
    },
  },
  // Personal
  {
    path: '/escritorio',
    name: 'escritorio',
    component: () => import('@/components/Escritorio'),
    meta: {
      title: 'Escritorio',
      rule: 'esPublico',
    },
  },
  {
    path: '/asistencias',
    name: 'asistencias',
    component: () => import('@/components/Asistencias/Asistencias'),
    meta: {
      title: 'Asistencia',
      rule: 'esUsuario',
    },
    children: [{
        path: 'crear',
        alias: '',
        name: 'crear-asistencia',
        component: () => import('@/components/Asistencias/CrearAsistencia'),
        meta: {
          title: 'Crear Asistencia',
          rule: 'esUsuario',
        },
      },
      {
        path: 'marcar',
        name: 'marcar-asistencia',
        component: () => import('@/components/Asistencias/MarcarAsistencia'),
        meta: {
          title: 'Marcar Asistencia',
          rule: 'esUsuario',
        },
      },
      {
        path: 'lista',
        name: 'lista-asistencia',
        component: () => import('@/components/Asistencias/ListaAsistencias'),
        meta: {
          title: 'Lista de Asistencias',
          rule: 'esUsuario',
        },
        children: [{
            path: 'personal/:personaId',
            alias: '',
            name: 'lista-personal-asistencia',
            component: () => import('@/components/Asistencias/ListaAsistencias'),
            props: true,
            meta: {
              title: 'Lista de Asistencias',
              rule: 'esUsuario',
            },
          },
          {
            path: 'general',
            name: 'lista-general-asistencia',
            component: () => import('@/components/Asistencias/ListaAsistencias'),
            meta: {
              title: 'Lista general de Asistencias',
              rule: 'esUsuario',
            },
          },
          {
            path: 'centro-costo/:centroCostoId',
            name: 'lista-centro-asistencia',
            props: true,
            component: () => import('@/components/Asistencias/ListaAsistencias'),
            meta: {
              title: 'Lista por centro de Asistencias',
              rule: 'esUsuario',
            },
          },
        ],
        props: true,
      },
    ],
  },
  {
    path: '/notas',
    name: 'notas',
    component: () => import('@/components/Notas/Notas'),
    meta: {
      title: 'Notas',
      rule: 'esUsuario',
    },
  },
  {
    path: '/notificaciones',
    name: 'notificaciones',
    component: () => import('@/components/Notificaciones/Notificaciones'),
    meta: {
      title: 'Notificaciones',
      rule: 'esUsuario',
    },
    children: [{
      path: 'crear',
      alias: '',
      name: 'crear-notificacion',
      component: () =>
        import('@/components/Notificaciones/CrearNotificacion'),
      meta: {
        title: 'Crear Notificación',
        rule: 'esUsuario',
      },
    }, ],
  },
  //Comercial
  {
    path: '/cotizaciones',
    name: 'cotizaciones',
    component: () => import('@/components/Cotizaciones/Cotizaciones'),
    meta: {
      title: 'Cotizaciones',
      rule: 'esUsuario',
    },
    children: [{
        path: 'lista/:cliente?',
        alias: '',
        name: 'lista-cotizaciones',
        component: () => import('@/components/Cotizaciones/ListaCotizaciones'),
        meta: {
          title: 'Lista de Cotizaciones',
          rule: 'esUsuario',
        },
      },
      {
        path: 'crear/:cliente?',
        name: 'crear-cotizacion',
        component: () => import('@/components/Cotizaciones/CrearCotizacion'),
        meta: {
          title: 'Crear nueva Cotización',
          rule: 'esUsuario',
        },
      },
      {
        path: 'editar/:id',
        name: 'editar-cotizacion',
        component: () => import('@/components/Cotizaciones/CrearCotizacion'),
        meta: {
          title: 'Editar Cotización',
          rule: 'esUsuario',
        },
        props: true,
      },
    ],
  },
  {
    path: '/presupuestos',
    name: 'presupuestos',
    component: () => import('@/components/Presupuestos/Presupuestos'),
    meta: {
      title: 'Presupuestos',
      rule: 'esUsuario',
    },
    children: [{
        path: 'lista',
        alias: '',
        name: 'lista-presupuestos',
        component: () => import('@/components/Presupuestos/ListaPresupuestos'),
        meta: {
          title: 'Lista de Presupuestos',
          rule: 'esUsuario',
        },
      },
      {
        path: 'crear',
        name: 'crear-presupuesto',
        component: () => import('@/components/Presupuestos/CrearPresupuesto'),
        meta: {
          title: 'Crear nuevo Presupuesto',
          rule: 'esUsuario',
        },
      },
      {
        path: 'editar/:id',
        name: 'editar-presupuesto',
        component: () => import('@/components/Presupuestos/CrearPresupuesto'),
        meta: {
          title: 'Editar Presupuesto',
          rule: 'esUsuario',
        },
        props: true,
      },
    ],
  },
  {
    path: '/clientes',
    name: 'clientes',
    component: () => import('@/components/Clientes/Clientes'),
    meta: {
      title: 'Clientes',
      rule: 'esUsuario',
    },
    children: [{
        path: 'lista',
        alias: '',
        name: 'lista-clientes',
        component: () => import('@/components/Clientes/ListaClientes'),
        meta: {
          title: 'Lista de Clientes',
          rule: 'esUsuario',
        },
      },
      {
        path: 'crear',
        name: 'crear-cliente',
        component: () => import('@/components/Clientes/CrearCliente'),
        meta: {
          title: 'Crear nuevo Cliente',
          rule: 'esUsuario',
        },
      },
      {
        path: 'editar/:id',
        name: 'editar-cliente',
        component: () => import('@/components/Clientes/CrearCliente'),
        meta: {
          title: 'Editar Cliente',
          rule: 'esUsuario',
        },
        props: true,
      },
    ],
  },
  //Producción
  {
    path: '/centros-de-costos',
    name: 'centros-de-costos',
    component: () => import('@/components/CentrosCostos/CentrosCostos'),
    meta: {
      title: 'Centros de Costos',
      rule: 'esUsuario',
    },
    children: [{
        path: 'lista',
        alias: '',
        name: 'lista-centros-de-costos',
        component: () =>
          import('@/components/CentrosCostos/ListaCentrosCostos'),
        children: [{
          path: 'completa',
          component: () =>
            import('@/components/CentrosCostos/ListaCentrosCostos'),
        }],
        meta: {
          title: 'Lista de Centros de Costos',
          rule: 'esUsuario',
        },
      },
      {
        path: 'crear/:cotizacion?',
        name: 'crear-centro-de-costos',
        component: () => import('@/components/CentrosCostos/CrearCentroCosto'),
        meta: {
          title: 'Crear nuevo Centro de Costos',
          rule: 'esUsuario',
        },
      },
      {
        path: 'editar/:id',
        name: 'editar-centro-de-costos',
        component: () => import('@/components/CentrosCostos/CrearCentroCosto'),
        meta: {
          title: 'Editar Centro de Costos',
          rule: 'esUsuario',
        },
        props: true,
      },
    ],
  },
  {
    path: '/proveedores',
    name: 'proveedores',
    component: () => import('@/components/Proveedores/Proveedores'),
    meta: {
      title: 'Proveedores',
      rule: 'esUsuario',
    },
    children: [{
        path: 'lista',
        alias: '',
        name: 'lista-proveedores',
        component: () => import('@/components/Proveedores/ListaProveedores'),
        meta: {
          title: 'Lista de Proveedores',
          rule: 'esUsuario',
        },
      },
      {
        path: 'crear',
        name: 'crear-proveedor',
        component: () => import('@/components/Proveedores/CrearProveedor'),
        meta: {
          title: 'Crear nuevo Proveedor',
          rule: 'esUsuario',
        },
      },
      {
        path: 'editar/:id',
        name: 'editar-proveedor',
        component: () => import('@/components/Proveedores/CrearProveedor'),
        meta: {
          title: 'Editar Proveedor',
          rule: 'esUsuario',
        },
        props: true,
      },
    ],
  },
  {
    path: '/gastos',
    name: 'gastos',
    component: () => import('@/components/Gastos/Gastos'),
    meta: {
      title: 'Gastos',
      rule: 'esUsuario',
    },
    children: [{
        path: 'lista',
        alias: '',
        name: 'lista-gastos',
        component: () => import('@/components/Gastos/ListaGastos'),
        meta: {
          title: 'Lista de Gastos',
          rule: 'esUsuario',
        },
      },
      {
        path: 'crear',
        name: 'crear-gasto',
        component: () => import('@/components/Gastos/CrearGasto'),
        meta: {
          title: 'Crear nuevo Gasto',
          rule: 'esUsuario',
        },
      },
      {
        path: 'editar/:id',
        name: 'editar-gasto',
        component: () => import('@/components/Gastos/CrearGasto'),
        meta: {
          title: 'Editar Proveedor',
          rule: 'esUsuario',
        },
        props: true,
      },
    ],
  },
  // Recursos Humanos
  {
    path: '/personas',
    name: 'personas',
    component: () => import('@/components/Personas/Personas'),
    meta: {
      title: 'Personas',
      rule: 'esUsuario',
    },
    children: [{
        path: 'lista',
        alias: '',
        name: 'lista-personas',
        component: () => import('@/components/Personas/ListaPersonas'),
        meta: {
          title: 'Lista de Personas',
          rule: 'esUsuario',
        },
      },
      {
        path: 'crear',
        name: 'crear-persona',
        component: () => import('@/components/Personas/CrearPersona'),
        meta: {
          title: 'Crear nueva Persona',
          rule: 'esUsuario',
        },
      },
      {
        path: 'editar/:id',
        name: 'editar-persona',
        component: () => import('@/components/Personas/CrearPersona'),
        meta: {
          title: 'Editar Persona',
          rule: 'esUsuario',
        },
        props: true,
      },
    ],
  },
  {
    path: '/vacaciones',
    name: 'vacaciones',
    component: () => import('@/components/Remuneraciones/Vacaciones'),
    meta: {
      title: 'Vacaciones',
      rule: 'esUsuario',
    },
    children: [{
        path: 'crear/:persona?',
        name: 'crear-vacacion',
        component: () => import('@/components/Remuneraciones/CrearVacacion'),
        meta: {
          title: 'Crear nuevas Vacaciones',
          rule: 'esUsuario',
        },
      },
      {
        path: 'lista',
        alias: '',
        name: 'lista-vacaciones',
        component: () => import('@/components/Remuneraciones/ListaVacaciones'),
        meta: {
          title: 'Lista de Vacaciones',
          rule: 'esUsuario',
        },
      },
      {
        path: 'editar/:id',
        name: 'editar-vacaciones',
        component: () => import('@/components/Remuneraciones/CrearVacacion'),
        meta: {
          title: 'Editar Vacaciones',
          rule: 'esUsuario',
        },
        props: true,
      },
    ],
  },
  {
    path: '/remuneraciones',
    name: 'remuneraciones',
    component: () => import('@/components/Remuneraciones/Remuneraciones'),
    meta: {
      title: 'Remuneraciones',
      rule: 'esUsuario',
    },
    children: [{
        path: 'haber-descuento',
        name: 'CrearHaberDescuento',
        component: () =>
          import('@/components/Remuneraciones/CrearHaberDescuento'),
        meta: {
          title: 'Crear nuevo Haber o Descuento',
          rule: 'esUsuario',
        },
      },
      {
        path: 'haber-descuento/editar/:id',
        name: 'editar-haber-descuento',
        component: () => import('@/components/Remuneraciones/CrearHaberDescuento'),
        meta: {
          title: 'Editar Haber o Descuento',
          rule: 'esUsuario',
        },
        props: true,
      },
    ],
  },
  // Contabilidad
  {
    path: '/plan-cuentas',
    name: 'plan-cuentas',
    component: () => import('@/components/Contabilidad/PlanCuentas'),
    meta: {
      title: 'Plan de Cuentas',
      rule: 'esUsuario',
    },
    children: [{
      path: 'lista',
      alias: '',
      name: 'lista-plan-cuentas',
      component: () => import('@/components/Contabilidad/ListaPlanCuentas'),
      meta: {
        title: 'Plan de Cuentas',
        rule: 'esUsuario',
      },
    }, ],
  },

  // Administración
  {
    path: '/balances',
    name: 'balances',
    component: () => import('@/components/Balances/Balances'),
    meta: {
      title: 'Balances',
      rule: 'esUsuario',
    },
  },
  {
    path: '/cheques',
    name: 'cheques',
    component: () => import('@/components/Cheques/Cheques'),
    meta: {
      title: 'Cheques',
      rule: 'esUsuario',
    },
    children: [{
        path: 'lista',
        alias: '',
        name: 'lista-cheques',
        component: () => import('@/components/Cheques/ListaCheques'),
        meta: {
          title: 'Lista de Cheques',
          rule: 'esUsuario',
        },
      },
      {
        path: 'crear',
        name: 'crear-cheque',
        component: () => import('@/components/Cheques/CrearCheque'),
        meta: {
          title: 'Crear nuevo Cheque',
          rule: 'esUsuario',
        },
      },
      {
        path: 'editar/:id',
        name: 'editar-cheque',
        component: () => import('@/components/Cheques/CrearCheque'),
        meta: {
          title: 'Editar Cheque',
          rule: 'esUsuario',
        },
        props: true,
      },
    ],
  },
  {
    path: '/fondos-por-rendir',
    name: 'fondos-por-rendir',
    component: () => import('@/components/FondosPorRendir/FondosPorRendir'),
    meta: {
      title: 'Fondos Por Rendir',
      rule: 'esUsuario',
    },
    children: [{
        path: 'lista',
        alias: '',
        name: 'lista-fondos-por-rendir',
        component: () =>
          import('@/components/FondosPorRendir/ListaFondosPorRendir'),
        meta: {
          title: 'Lista de Fondos Por Rendir',
          rule: 'esUsuario',
        },
      },
      {
        path: 'crear',
        name: 'crear-fondo-por-rendir',
        component: () =>
          import('@/components/FondosPorRendir/CrearFondoPorRendir'),
        meta: {
          title: 'Crear nuevo Fondo Por Rendir',
          rule: 'esUsuario',
        },
      },
      {
        path: 'editar/:id',
        name: 'editar-fondo-por-rendir',
        component: () => import('@/components/FondosPorRendir/CrearFondoPorRendir'),
        meta: {
          title: 'Editar Fondo Por Rendir',
          rule: 'esUsuario',
        },
        props: true,
      },
    ],
  },
  {
    path: '/prestamos',
    name: 'prestamos',
    component: () => import('@/components/Prestamos/Prestamos'),
    meta: {
      title: 'Préstamos',
      rule: 'esUsuario',
    },
    children: [{
        path: 'lista',
        alias: '',
        name: 'lista-prestamos',
        component: () => import('@/components/Prestamos/ListaPrestamos'),
        meta: {
          title: 'Lista de Préstamos',
          rule: 'esUsuario',
        },
      },
      {
        path: 'crear',
        name: 'crear-prestamo',
        component: () => import('@/components/Prestamos/CrearPrestamo'),
        meta: {
          title: 'Crear nuevo Préstamo',
          rule: 'esUsuario',
        },
      },
      {
        path: 'editar/:id',
        name: 'editar-prestamo',
        component: () => import('@/components/Prestamos/CrearPrestamo'),
        meta: {
          title: 'Editar Préstamo',
          rule: 'esUsuario',
        },
        props: true,
      },
    ],
  },

  // Orgánico
  {
    path: '/actas',
    name: 'actas',
    component: () => import('@/components/Actas/Actas'),
    meta: {
      title: 'Actas',
      rule: 'esUsuario',
    },
    children: [{
        path: 'lista',
        alias: '',
        name: 'lista-actas',
        component: () => import('@/components/Actas/ListaActas'),
        meta: {
          title: 'Lista de Actas',
          rule: 'esUsuario',
        },
      },
      {
        path: 'ver/:id',
        name: 'ver-acta',
        component: () => import('@/components/Actas/VerActa'),
        meta: {
          title: 'Ver Acta',
          rule: 'esUsuario',
        },
        props: true,
      },
      {
        path: 'crear',
        name: 'crear-acta',
        component: () => import('@/components/Actas/CrearActa'),
        meta: {
          title: 'Crear nueva Acta',
          rule: 'esUsuario',
        },
      },
      {
        path: 'editar/:id',
        name: 'editar-acta',
        component: () => import('@/components/Actas/CrearActa'),
        meta: {
          title: 'Editar Acta',
          rule: 'esUsuario',
        },
        props: true,
      },
    ],
  },
  // Usuarios
  {
    path: '/usuarios',
    name: 'usuarios',
    component: () => import('@/components/Usuarios/Usuarios'),
    meta: {
      title: 'Usuarios',
      rule: 'esUsuario',
    },
    children: [{
        path: 'lista',
        alias: '',
        name: 'lista-usuarios',
        component: () => import('@/components/Usuarios/ListaUsuarios'),
        meta: {
          title: 'Lista de Usuarios',
          rule: 'esUsuario',
        },
      },
      {
        path: 'crear',
        name: 'crear-usuario',
        component: () => import('@/components/Usuarios/CrearUsuario'),
        meta: {
          title: 'Crear nuevo Usuario',
          rule: 'esUsuario',
        },
      },
      {
        path: 'editar/:id',
        name: 'editar-usuario',
        component: () => import('@/components/Usuarios/CrearUsuario'),
        meta: {
          title: 'Editar Usuario',
          rule: 'esUsuario',
        },
        props: true,
      },
      /*{
        path: 'roles',
        component: Roles,
        children: [{
            path: 'lista',
            alias: '',
            name: 'ListaRoles',
            component: ListaRoles,
          },
          {
            path: 'crear',
            name: 'CrearRol',
            component: CrearRol,
          }
        ]
      }*/
    ],
  },
  {
    path: '/configuracion',
    name: 'configuracion',
    component: () => import('@/components/Configuracion/Configuracion'),
    meta: {
      title: 'Configuración',
      rule: 'esUsuario',
    },
  },
  {
    path: '*',
    name: 'error-404',
    component: () => import('@/components/Errores/404'),
    meta: {
      title: 'Página no encontrada',
      rule: 'esUsuario',
    },
  },
]

export default routes
