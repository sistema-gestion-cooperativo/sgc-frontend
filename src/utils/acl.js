import Vue from 'vue'
import { AclInstaller, AclCreate, AclRule } from '@/utils/acl/index.js'
import router from '@/router'
import store from '@/store'

Vue.use(AclInstaller)

/*
Reglas:
- administracion
- comercial
- recursos_humanos
- produccion
- jefe_de_obra
- socio
- trabajador
- publico
*/
export default new AclCreate({
  initial: 'publico',
  notfound: '/error',
  router,
  store,
  acceptLocalRules: false,
  globalRules: {
    esAdministracion: new AclRule('administracion').generate(),
    esComercial: new AclRule('comercial').or('administracion').generate(),
    esRecursosHumanos: new AclRule('recursos_humanos')
      .or('administracion')
      .generate(),
    esProduccion: new AclRule('produccion').or('administracion').generate(),
    esJefeDeObra: new AclRule('jefe_de_obra')
      .or('produccion')
      .or('administracion')
      .generate(),
    esSocio: new AclRule('socio')
      .or('jefe_de_obra')
      .or('produccion')
      .or('comercial')
      .or('recursos_humanos')
      .or('administracion')
      .generate(),
    esUsuario: new AclRule('trabajador')
      .or('socio')
      .or('jefe_de_obra')
      .or('produccion')
      .or('comercial')
      .or('recursos_humanos')
      .or('administracion')
      .generate(),
    esPublico: new AclRule('publico')
      .or('socio')
      .or('jefe_de_obra')
      .or('produccion')
      .or('comercial')
      .or('recursos_humanos')
      .or('administracion')
      .generate(),
  },
})
