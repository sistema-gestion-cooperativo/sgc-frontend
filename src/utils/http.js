import Vue from 'vue'
import axios from 'axios'
import store from '@/store'
const env = require('@/.env.js')

var axiosInstance = axios.create({
  baseURL: env.baseURL,
  timeout: 10000,
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    Accept: 'application/json',
  },
})

axiosInstance.interceptors.request.use(
  request => {
    if (store.getters.estaAutenticado) {
      request.headers['Authorization'] = store.getters.accessToken
    }
    return request
  },
  error => {
    console.error('HTTP: Error request', error.request)
    return Promise.reject(error)
  }
)

axiosInstance.interceptors.response.use(
  response => response,
  error => {
    if (error.response.status === 408 || error.code === 'ECONNABORTED') { //TODO: Con este interceptor cambiar mensaje de timeout 8s (ahora 10)
      console.log('A time happend on url', error)
    }
    if (error.response) {
      let mensaje_error
      switch (error.response.status) {
        case 400:
          mensaje_error = 'Faltan datos.'
          break
        case 401:
          console.log('retry')
          if (store.getters.estaAutenticado && !error.config._retry) {
            error.config._retry = true
            return axiosInstance
              .post('/oauth/token', {
                grant_type: 'refresh_token',
                client_id: store.getters.clientId,
                client_secret: store.getters.clientSecret,
                refresh_token: store.getters.refreshToken,
                scope: '',
              })
              .then(response => {
                // store.dispatch('autenticar', response.data)
                store.dispatch('cambiarAutenticacion', response.data)
                axiosInstance.defaults.headers.common['Authorization'] =
                  store.getters.accessToken
                error.config.headers['Authorization'] =
                  store.getters.accessToken
                return axiosInstance(error.config)
              })
          } else {
            store.dispatch('cerrarSesion')
          }
          mensaje_error = 'Las credenciales no coinciden o no existen.'
          break
        case 403:
          mensaje_error = 'No estás autorizado.'
          break
        case 404:
          if (!error.config._retry) error.config._retry = true
          mensaje_error = 'La página solicitada no existe.'
          break
        case 405:
          mensaje_error = 'Método no permitido.'
          break
        case 500:
          mensaje_error = 'Error en el servidor.'
          break
        default:
          mensaje_error = 'Error desconocido.'
          break
      }
      error.mensaje = mensaje_error
    } else if (error.request) {
      // The request was made but no response was received
      console.error('HTTP: Error response.request', error.request)
    } else {
      // Something happened in setting up the request that triggered an Error
      console.error('HTTP: Error message', error.message)
    }
    return Promise.reject(error)
  }
)

Object.defineProperties(Vue.prototype, {
  axios: {
    get() {
      return axiosInstance
    },
  },
  $http: {
    get() {
      return axiosInstance
    },
  },
})

export default axiosInstance
