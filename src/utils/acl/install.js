// @ts-check
import { VueConstructor } from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import { register } from './mixin'

/**
 * @typedef {Object} VueAclParams
 * @property {string|Array} initial
 * @property {VueRouter} router
 * @property {Vuex} store
 * @property {string} notfound
 * @property {Object} [globalRules]
 * @property {Boolean} [acceptLocalRules]
 */

/**
 * Function for install plugin with Vue.use
 * @function
 * @param {VueConstructor} _Vue
 * @param {VueAclParams} options
 */
export const _install = (_Vue, options) => {
  const {
    initial,
    acceptLocalRules,
    globalRules,
    router,
    store,
    notfound,
  } = options

  _Vue.mixin(
    register(
      initial || 'public',
      acceptLocalRules || false,
      globalRules || {},
      router,
      store,
      notfound
    )
  )
}
