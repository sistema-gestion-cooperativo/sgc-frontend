require('../node_modules/vuetify/src/stylus/app.styl')

import Vue from 'vue'

if (process.env.NODE_ENV === 'production') {
  Vue.config.productionTip = true
  Vue.config.devtools = false
  /** Sentry */
  let Raven = require('raven-js')
  let RavenVue = require('raven-js/plugins/vue')
  Raven.config('https://041eeda81f9344adb5b0ac6e3ed89c93@sentry.io/277592')
    .addPlugin(RavenVue, Vue)
    .install()
  /** **/
} else {
  Vue.config.productionTip = false
  Vue.config.devtools = true
}
import {
  Vuetify,
  VApp,
  VAlert,
  VAutocomplete,
  VAvatar,
  VBadge,
  VBtn,
  VBtnToggle,
  VCard,
  VCheckbox,
  VChip,
  VDataIterator,
  VDataTable,
  VDatePicker,
  VDialog,
  VDivider,
  VExpansionPanel,
  VFooter,
  VForm,
  VGrid,
  VIcon,
  VList,
  VMenu,
  VNavigationDrawer,
  VPagination,
  // VProgressCircular,
  VProgressLinear,
  VRadioGroup,
  VSelect,
  // VSlider,
  VSnackbar,
  VSpeedDial,
  VStepper,
  VSubheader,
  VSwitch,
  VSystemBar,
  VTabs,
  VTextarea,
  VTextField,
  VTimePicker,
  VToolbar,
  VTooltip,
  transitions
} from 'vuetify'
import directives from 'vuetify/es5/directives'
import App from '@/App'
import http from './utils/http.js'
import acl from './utils/acl.js'
import router from '@/router'
import store from '@/store'

Vue.use(Vuetify, {
  theme: {
    primary: '#227b89',
    secondary: '#0097a7',
    accent: '#82B1ff',
    error: '#ff5252',
    info: '#2196f3',
    success: '#4CaF50',
    warning: '#ffc107',
  },
  components: {
    VApp,
    VAlert,
    VAutocomplete,
    VAvatar,
    VBadge,
    VBtn,
    VBtnToggle,
    VCard,
    VCheckbox,
    VChip,
    VDataIterator,
    VDataTable,
    VDatePicker,
    VDialog,
    VDivider,
    VExpansionPanel,
    VFooter,
    VForm,
    VGrid,
    VIcon,
    VList,
    VMenu,
    VNavigationDrawer,
    VPagination,
    // VProgressCircular,
    VProgressLinear,
    VRadioGroup,
    VSelect,
    // VSlider,
    VSnackbar,
    VSpeedDial,
    VStepper,
    VSubheader,
    VSwitch,
    VSystemBar,
    VTabs,
    VTextarea,
    VTextField,
    VTimePicker,
    VToolbar,
    VTooltip,
    transitions
  },
  directives
})

new Vue({
  store,
  router,
  acl,
  el: '#app',
  components: {
    App,
  },
  render: createElement => createElement(App),
})
