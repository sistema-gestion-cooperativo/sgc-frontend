export default function(
  codigo,
  numero,
  nombre_presentacion,
  cantidad,
  unidad,
  precio,
  descripcion,
  presupuesto
) {
  this.codigo = null
  this.numero = null
  this.nombre_presentacion = null
  this.cantidad = 1
  this.unidad = null
  this.monto_presupuesto = null
  this.precio_cotizacion = null
  this.descripcion = ''
  this.presupuesto = {
    materiales: 0,
    mano_de_obra: [{}],
    excedentes: 0,
    hospedaje: 0,
    viatico: 0,
    traslado: 0,
    otros: 0,
    neto: 0,
  }

  this.netoPresupuesto = () => {
    let total_mano_de_obra = this.presupuesto.mano_de_obra.reduce(
      (suma, valor) => {
        return suma + parseInt(valor.monto_horas, 10) || 0
      },
      0
    )

    return (
      (parseInt(this.presupuesto.materiales, 10) || 0) +
      (parseInt(this.presupuesto.excedentes, 10) || 0) +
      (parseInt(this.presupuesto.hospedaje, 10) || 0) +
      (parseInt(this.presupuesto.viatico, 10) || 0) +
      (parseInt(this.presupuesto.traslado, 10) || 0) +
      (parseInt(this.presupuesto.otros, 10) || 0) +
      total_mano_de_obra
    )
  }

  this.totalPresupuesto = (utilidad) => {
    return this.netoPresupuesto() +(this.netoPresupuesto() * utilidad / 100)
  }

  this.netoCotizacion = (utilidad) => {
    return this.totalPresupuesto(utilidad) * this.cantidad
  }
}
