export default function() {
  this.rut = null
  this.nombre = null
  this.nombre_presentacion = null
  this.giro = null
  this.tipo = null
  this.rubro = null
  this.descuento = null
  this.telefono = null
  this.correo = null
  this.redes_sociales = null
  this.comentarios = null
  this.direcciones = [{}]
  this.contactos = [{}]
  this.redes_sociales = [{}]

  this.nuevoContacto = function() {
    this.contactos.push({})
  }
}
